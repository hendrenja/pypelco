# Example program to control a PELCO-D-type camera mount,
# specificall this seems to work well with a
#   https://www.aliexpress.com/item/Pan-Tilt-motorized-rotation-bracket-stand-holder-PELCO-D-control-for-CCTV-IP-camera-module-RS232/32827664380.html

import serial
import codecs
import time
from pelco_mount import *

PORT = '/dev/ttyUSB0'
s = serial.Serial(PORT,timeout=.1,baudrate=9600)
print(s)
m = pelco_mount(PORT)
m._adr=0

s.write(m.msg_test_init())

for aux_id in range(0,8):
    print(f"AUX [{aux_id}] ON")
    s.write(m.msg_set_aux(aux_id))
    m.set_aux(aux_id)
    time.sleep(2)
    print(f"AUX [{aux_id}] OFF")
    s.write(m.msg_clear_aux(aux_id))
    time.sleep(1)

#s.write(m.msg_pan_right())

#s.write(m.msg_go_pre(18))
#s.write(m.msg_go_home())


# for speed in (SPEED_SLOW,SPEED_MEDIUM,SPEED_HIGH):
    # s.write(m.msg_set_speed(speed))
    # s.write(m.msg_pan_left())
    # time.sleep(1)
    # s.write(m.msg_pan_right())
    # time.sleep(1)

a = s.read(100)
print(a)

s.close()
